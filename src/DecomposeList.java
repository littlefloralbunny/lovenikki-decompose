import java.util.Scanner;
import java.util.ArrayList;


public class DecomposeList {


	public static ArrayList<String> getInputItems(Scanner input) {
		ArrayList<String> itemArray = new ArrayList<String>();
		String itemName = ""; 
		while(!itemName.equals("-1")) {
			itemName = input.nextLine();
			itemName = itemName.toLowerCase();
			if (!itemName.equals("-1")) {
				itemArray.add(itemName);
			}
		}
		return itemArray;
	}
	
	public static ArrayList<String> calcItemsToDecompose(ArrayList<String> neededItems, ArrayList<String> decomposeItems) {
		ArrayList<String> decomposeTheseItems = new ArrayList<String>();
		
		for (int i = 0; i < decomposeItems.size(); i++) {
			if (!neededItems.contains(decomposeItems.get(i))) {
				decomposeTheseItems.add(decomposeItems.get(i));
			}
		}
		
		return decomposeTheseItems;
	}
	
	public static void searchDecomposeItems(ArrayList<String> decomposeTheseItems, Scanner input) {
		String itemName = ""; 
		while (!itemName.equals("-1")) {
			itemName = input.nextLine();
			itemName = itemName.toLowerCase();
			if (decomposeTheseItems.contains(itemName)) {
				System.out.println("Yes! Decompose this item.");
			}
			else {
				System.out.println("No! You need this item.");
			}
		}
	}
	
	// for testing purposes only
	public static void printArrayList(ArrayList<String> array) {
		for (int i = 0; i < array.size(); i++) {
			System.out.println(array.get(i));
		}
	}

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Please enter each name of the items required for the sets you are crafting (ie 'Sweet Jasmine White'). When you are done, enter -1.");
		ArrayList<String> neededItems = getInputItems(input);
		
		// printArrayList(neededItems);
		
		System.out.println("Please enter each name of the items in your decompose inventory. When you are done, enter -1.");
		ArrayList<String> decomposeItems = getInputItems(input);
		
		// printArrayList(decomposeItems);
		
		ArrayList<String> decomposeTheseItems = calcItemsToDecompose(neededItems, decomposeItems);
		
		System.out.println("Now enter in an item name to find out whether you should keep or decompose it. When you are done, enter -1.");
		searchDecomposeItems(decomposeTheseItems, input);
		
		// printArrayList(decomposeTheseItems);
		
		System.out.println("All done :)");
		input.close();
	}
}
