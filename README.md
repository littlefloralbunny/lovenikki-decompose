# LoveNikki-Decompose
----

## Why did I make this program?

I play the mobile game Love Nikki-Dress UP Queen; in it you can decompose clothes for resources to craft other clothes. However, I didn't want to decompose clothes that I needed for sets I wanted to craft.
I wrote this program to work out which clothes I can safely decompose for more resources.

## What does it do?

This program will first ask you to enter the names of the clothes you need for the sets you want to craft. Then, it will ask you to input the names of the clothes that appear in your decompose menu. Once both
have been entered, you can enter in a name of clothing and the program will let you know whether you should decompose or keep the item.

## Language

This program was written in Java in the Eclipse IDE.

## How to Run

To run this program, execute the windows batch file (.bat) and follow the on screen instructions.